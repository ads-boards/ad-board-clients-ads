import { Descriptor } from 'pip-services3-commons-node';
import { Factory } from 'pip-services3-components-node';

import { AdsNullClientV1 } from '../version1/AdsNullClientV1';
import { AdsDirectClientV1 } from '../version1/AdsDirectClientV1';
import { AdsHttpClientV1 } from '../version1/AdsHttpClientV1';


export class AdsClientFactory extends Factory {
	public static Descriptor: Descriptor = new Descriptor('ad-board-ads', 'factory', 'default', 'default', '1.0');
	public static NullClientV1Descriptor = new Descriptor('ad-board-ads', 'client', 'null', 'default', '1.0');
	public static DirectClientV1Descriptor = new Descriptor('ad-board-ads', 'client', 'direct', 'default', '1.0');
	public static HttpClientV1Descriptor = new Descriptor('ad-board-ads', 'client', 'http', 'default', '1.0');

	
	constructor() {
		super();

		this.registerAsType(AdsClientFactory.NullClientV1Descriptor, AdsNullClientV1);
		this.registerAsType(AdsClientFactory.DirectClientV1Descriptor, AdsDirectClientV1);
		this.registerAsType(AdsClientFactory.HttpClientV1Descriptor, AdsHttpClientV1);
	}
	
}
