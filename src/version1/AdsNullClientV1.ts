import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams} from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';

import { IAdsClientV1 } from './IAdsClientV1';
import { AdV1 } from './AdV1';

export class AdsNullClientV1 implements IAdsClientV1 {
            
    public getAds(correlationId: string, filter: FilterParams, paging: PagingParams, 
        callback: (err: any, page: DataPage<AdV1>) => void): void {
        callback(null, new DataPage<AdV1>([], 0));
    }

    public getAdById(correlationId: string, adId: string, 
        callback: (err: any, item: AdV1) => void): void {
        callback(null, null);
    }

    public createAd(correlationId: string, ad: AdV1, 
        callback: (err: any, item: AdV1) => void): void {
        callback(null, ad);
    }

    public updateAd(correlationId: string, ad: AdV1, 
        callback: (err: any, item: AdV1) => void): void {
        callback(null, ad);
    }

    public deleteAdById(correlationId: string, adId: string,
        callback: (err: any, item: AdV1) => void): void {
        callback(null, null);
    }
}