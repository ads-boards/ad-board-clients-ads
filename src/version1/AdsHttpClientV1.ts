import { ConfigParams } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { CommandableHttpClient } from 'pip-services3-rpc-node';

import { AdV1 } from './AdV1';
import { IAdsClientV1 } from './IAdsClientV1';

export class AdsHttpClientV1 extends CommandableHttpClient implements IAdsClientV1 {       
    
    constructor(config?: any) {
        super('v1/ads');

        if (config != null)
            this.configure(ConfigParams.fromValue(config));
    }
                
    public getAds(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<AdV1>) => void): void {
        this.callCommand( 
            'get_ads', 
            correlationId,
            {
                filter: filter,
                paging: paging
            }, 
            callback
        );
    }

    public getAdById(correlationId: string, adId: string,
        callback: (err: any, item: AdV1) => void): void {
        this.callCommand( 
            'get_ad_by_id',
            correlationId,
            {
                ad_id: adId
            }, 
            callback
        );        
    }

    public createAd(correlationId: string, ad: AdV1,
        callback: (err: any, item: AdV1) => void): void {
        this.callCommand(
            'create_ad',
            correlationId,
            {
                ad: ad
            }, 
            callback
        );
    }

    public updateAd(correlationId: string, ad: AdV1,
        callback: (err: any, item: AdV1) => void): void {
        this.callCommand(
            'update_ad', 
            correlationId,
            {
                ad: ad
            }, 
            callback
        );
    }

    public deleteAdById(correlationId: string, adId: string,
        callback: (err: any, item: AdV1) => void): void {
        this.callCommand(
            'delete_ad_by_id', 
            correlationId,
            {
                ad_id: adId
            }, 
            callback
        );
    }
    
}
