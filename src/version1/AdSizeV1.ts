export class AdSizeV1 {
    public static Small:string = "small";
    public static Medium:string = "medium";
    public static Large:string = "large";
}