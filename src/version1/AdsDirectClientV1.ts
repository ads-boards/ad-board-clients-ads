import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams} from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { DirectClient } from 'pip-services3-rpc-node';

import { IAdsClientV1 } from './IAdsClientV1';

import { AdV1 } from './AdV1';

export class AdsDirectClientV1 extends DirectClient<any> implements IAdsClientV1 {
            
    public constructor() {
        super();
        this._dependencyResolver.put('controller', new Descriptor("ad-board-ads", "controller", "*", "*", "*"))
    }

    public getAds(correlationId: string, filter: FilterParams, paging: PagingParams, 
        callback: (err: any, page: DataPage<AdV1>) => void): void {
        let timing = this.instrument(correlationId, 'ads.get_ads');
        this._controller.getAds(correlationId, filter, paging, (err, page) => {
            timing.endTiming();
            callback(err, page);
        });
    }

    public getAdById(correlationId: string, adId: string, 
        callback: (err: any, item: AdV1) => void): void {
        let timing = this.instrument(correlationId, 'ads.get_ad_by_id');
        this._controller.getAdById(correlationId, adId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public createAd(correlationId: string, ad: AdV1, 
        callback: (err: any, item: AdV1) => void): void {
        let timing = this.instrument(correlationId, 'ads.create_ad');
        this._controller.createAd(correlationId, ad, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public updateAd(correlationId: string, ad: AdV1, 
        callback: (err: any, ad: AdV1) => void): void {
        let timing = this.instrument(correlationId, 'ads.update_ad');
        this._controller.updateAd(correlationId, ad, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public deleteAdById(correlationId: string, adId: string,
        callback: (err: any, item: AdV1) => void): void {
        let timing = this.instrument(correlationId, 'ads.delete_ad_by_id');
        this._controller.deleteAdById(correlationId, adId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
}