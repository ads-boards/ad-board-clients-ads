import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { DirectClient } from 'pip-services3-rpc-node';
import { IAdsClientV1 } from './IAdsClientV1';
import { AdV1 } from './AdV1';
export declare class AdsDirectClientV1 extends DirectClient<any> implements IAdsClientV1 {
    constructor();
    getAds(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<AdV1>) => void): void;
    getAdById(correlationId: string, adId: string, callback: (err: any, item: AdV1) => void): void;
    createAd(correlationId: string, ad: AdV1, callback: (err: any, item: AdV1) => void): void;
    updateAd(correlationId: string, ad: AdV1, callback: (err: any, ad: AdV1) => void): void;
    deleteAdById(correlationId: string, adId: string, callback: (err: any, item: AdV1) => void): void;
}
