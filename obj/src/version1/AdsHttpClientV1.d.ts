import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { CommandableHttpClient } from 'pip-services3-rpc-node';
import { AdV1 } from './AdV1';
import { IAdsClientV1 } from './IAdsClientV1';
export declare class AdsHttpClientV1 extends CommandableHttpClient implements IAdsClientV1 {
    constructor(config?: any);
    getAds(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<AdV1>) => void): void;
    getAdById(correlationId: string, adId: string, callback: (err: any, item: AdV1) => void): void;
    createAd(correlationId: string, ad: AdV1, callback: (err: any, item: AdV1) => void): void;
    updateAd(correlationId: string, ad: AdV1, callback: (err: any, item: AdV1) => void): void;
    deleteAdById(correlationId: string, adId: string, callback: (err: any, item: AdV1) => void): void;
}
