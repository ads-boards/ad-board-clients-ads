"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsNullClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
class AdsNullClientV1 {
    getAds(correlationId, filter, paging, callback) {
        callback(null, new pip_services3_commons_node_1.DataPage([], 0));
    }
    getAdById(correlationId, adId, callback) {
        callback(null, null);
    }
    createAd(correlationId, ad, callback) {
        callback(null, ad);
    }
    updateAd(correlationId, ad, callback) {
        callback(null, ad);
    }
    deleteAdById(correlationId, adId, callback) {
        callback(null, null);
    }
}
exports.AdsNullClientV1 = AdsNullClientV1;
//# sourceMappingURL=AdsNullClientV1.js.map