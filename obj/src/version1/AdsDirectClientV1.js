"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsDirectClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class AdsDirectClientV1 extends pip_services3_rpc_node_1.DirectClient {
    constructor() {
        super();
        this._dependencyResolver.put('controller', new pip_services3_commons_node_1.Descriptor("ad-board-ads", "controller", "*", "*", "*"));
    }
    getAds(correlationId, filter, paging, callback) {
        let timing = this.instrument(correlationId, 'ads.get_ads');
        this._controller.getAds(correlationId, filter, paging, (err, page) => {
            timing.endTiming();
            callback(err, page);
        });
    }
    getAdById(correlationId, adId, callback) {
        let timing = this.instrument(correlationId, 'ads.get_ad_by_id');
        this._controller.getAdById(correlationId, adId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    createAd(correlationId, ad, callback) {
        let timing = this.instrument(correlationId, 'ads.create_ad');
        this._controller.createAd(correlationId, ad, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    updateAd(correlationId, ad, callback) {
        let timing = this.instrument(correlationId, 'ads.update_ad');
        this._controller.updateAd(correlationId, ad, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    deleteAdById(correlationId, adId, callback) {
        let timing = this.instrument(correlationId, 'ads.delete_ad_by_id');
        this._controller.deleteAdById(correlationId, adId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
}
exports.AdsDirectClientV1 = AdsDirectClientV1;
//# sourceMappingURL=AdsDirectClientV1.js.map