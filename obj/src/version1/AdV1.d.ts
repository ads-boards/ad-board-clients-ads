import { IStringIdentifiable } from 'pip-services3-commons-node';
export declare class AdV1 implements IStringIdentifiable {
    id: string;
    publish_group_ids: string[];
    title: string;
    text: string;
    img: string;
    text_color: string;
    text_size: number;
    background_color: string;
    critical: boolean;
    deleted: boolean;
    size: string;
    disabled: boolean;
    create_time?: Date;
    end_time?: Date;
}
