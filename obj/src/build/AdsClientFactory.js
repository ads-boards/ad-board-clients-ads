"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsClientFactory = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const AdsNullClientV1_1 = require("../version1/AdsNullClientV1");
const AdsDirectClientV1_1 = require("../version1/AdsDirectClientV1");
const AdsHttpClientV1_1 = require("../version1/AdsHttpClientV1");
class AdsClientFactory extends pip_services3_components_node_1.Factory {
    constructor() {
        super();
        this.registerAsType(AdsClientFactory.NullClientV1Descriptor, AdsNullClientV1_1.AdsNullClientV1);
        this.registerAsType(AdsClientFactory.DirectClientV1Descriptor, AdsDirectClientV1_1.AdsDirectClientV1);
        this.registerAsType(AdsClientFactory.HttpClientV1Descriptor, AdsHttpClientV1_1.AdsHttpClientV1);
    }
}
exports.AdsClientFactory = AdsClientFactory;
AdsClientFactory.Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'factory', 'default', 'default', '1.0');
AdsClientFactory.NullClientV1Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'client', 'null', 'default', '1.0');
AdsClientFactory.DirectClientV1Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'client', 'direct', 'default', '1.0');
AdsClientFactory.HttpClientV1Descriptor = new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'client', 'http', 'default', '1.0');
//# sourceMappingURL=AdsClientFactory.js.map