import { IAdsClientV1 } from '../../src/version1/IAdsClientV1';
export declare class AdsClientFixtureV1 {
    private _client;
    constructor(client: IAdsClientV1);
    testCrudOperations(done: any): void;
}
