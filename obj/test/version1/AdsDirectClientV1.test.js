"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let assert = require('chai').assert;
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const ad_board_services_ads_node_1 = require("ad-board-services-ads-node");
const ad_board_services_ads_node_2 = require("ad-board-services-ads-node");
const AdsDirectClientV1_1 = require("../../src/version1/AdsDirectClientV1");
const AdsClientFixtureV1_1 = require("./AdsClientFixtureV1");
suite('AdsDirectClientV1', () => {
    let client;
    let fixture;
    suiteSetup((done) => {
        let logger = new pip_services3_components_node_1.ConsoleLogger();
        let persistence = new ad_board_services_ads_node_1.AdsMemoryPersistence();
        let controller = new ad_board_services_ads_node_2.AdsController();
        let references = pip_services3_commons_node_2.References.fromTuples(new pip_services3_commons_node_1.Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger, new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'controller', 'default', 'default', '1.0'), controller);
        controller.setReferences(references);
        client = new AdsDirectClientV1_1.AdsDirectClientV1();
        client.setReferences(references);
        fixture = new AdsClientFixtureV1_1.AdsClientFixtureV1(client);
        client.open(null, done);
    });
    suiteTeardown((done) => {
        client.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
});
//# sourceMappingURL=AdsDirectClientV1.test.js.map