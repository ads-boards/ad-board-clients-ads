"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let assert = require('chai').assert;
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const ad_board_services_ads_node_1 = require("ad-board-services-ads-node");
const ad_board_services_ads_node_2 = require("ad-board-services-ads-node");
const ad_board_services_ads_node_3 = require("ad-board-services-ads-node");
const AdsHttpClientV1_1 = require("../../src/version1/AdsHttpClientV1");
const AdsClientFixtureV1_1 = require("./AdsClientFixtureV1");
var httpConfig = pip_services3_commons_node_2.ConfigParams.fromTuples("connection.protocol", "http", "connection.host", "localhost", "connection.port", 3000);
suite('AdsRestClientV1', () => {
    let service;
    let client;
    let fixture;
    suiteSetup((done) => {
        let logger = new pip_services3_components_node_1.ConsoleLogger();
        let persistence = new ad_board_services_ads_node_1.AdsMemoryPersistence();
        let controller = new ad_board_services_ads_node_2.AdsController();
        service = new ad_board_services_ads_node_3.AdsHttpServiceV1();
        service.configure(httpConfig);
        let references = pip_services3_commons_node_3.References.fromTuples(new pip_services3_commons_node_1.Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger, new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'controller', 'default', 'default', '1.0'), controller, new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'service', 'http', 'default', '1.0'), service);
        controller.setReferences(references);
        service.setReferences(references);
        client = new AdsHttpClientV1_1.AdsHttpClientV1();
        client.setReferences(references);
        client.configure(httpConfig);
        fixture = new AdsClientFixtureV1_1.AdsClientFixtureV1(client);
        service.open(null, (err) => {
            client.open(null, done);
        });
    });
    suiteTeardown((done) => {
        client.close(null);
        service.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
});
//# sourceMappingURL=AdsHttpClientV1.test.js.map