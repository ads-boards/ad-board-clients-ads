let assert = require('chai').assert;
let async = require('async');

import { Descriptor } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';
import { ConsoleLogger } from 'pip-services3-components-node';

import { AdsMemoryPersistence } from 'ad-board-services-ads-node';
import { AdsController } from 'ad-board-services-ads-node';
import { AdsHttpServiceV1 } from 'ad-board-services-ads-node';
import { AdsHttpClientV1 } from '../../src/version1/AdsHttpClientV1';
import { AdsClientFixtureV1 } from './AdsClientFixtureV1';

var httpConfig = ConfigParams.fromTuples(
    "connection.protocol", "http",
    "connection.host", "localhost",
    "connection.port", 3000
);

suite('AdsRestClientV1', ()=> {
    let service: AdsHttpServiceV1;
    let client: AdsHttpClientV1;
    let fixture: AdsClientFixtureV1;

    suiteSetup((done) => {
        let logger = new ConsoleLogger();
        let persistence = new AdsMemoryPersistence();
        let controller = new AdsController();

        service = new AdsHttpServiceV1();
        service.configure(httpConfig);

        let references: References = References.fromTuples(
            new Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger,
            new Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '1.0'), persistence,
            new Descriptor('ad-board-ads', 'controller', 'default', 'default', '1.0'), controller,
            new Descriptor('ad-board-ads', 'service', 'http', 'default', '1.0'), service
        );
        controller.setReferences(references);
        service.setReferences(references);

        client = new AdsHttpClientV1();
        client.setReferences(references);
        client.configure(httpConfig);

        fixture = new AdsClientFixtureV1(client);

        service.open(null, (err) => {
            client.open(null, done);
        });
    });
    
    suiteTeardown((done) => {
        client.close(null);
        service.close(null, done);
    });

    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

});
