let assert = require('chai').assert;
let async = require('async');

import { Descriptor } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';
import { ConsoleLogger } from 'pip-services3-components-node';

import { AdsMemoryPersistence } from 'ad-board-services-ads-node';
import { AdsController } from 'ad-board-services-ads-node';
import { AdsDirectClientV1 } from '../../src/version1/AdsDirectClientV1';
import { AdsClientFixtureV1 } from './AdsClientFixtureV1';

suite('AdsDirectClientV1', ()=> {
    let client: AdsDirectClientV1;
    let fixture: AdsClientFixtureV1;

    suiteSetup((done) => {
        let logger = new ConsoleLogger();
        let persistence = new AdsMemoryPersistence();
        let controller = new AdsController();

        let references: References = References.fromTuples(
            new Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger,
            new Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '1.0'), persistence,
            new Descriptor('ad-board-ads', 'controller', 'default', 'default', '1.0'), controller,
        );
        controller.setReferences(references);

        client = new AdsDirectClientV1();
        client.setReferences(references);

        fixture = new AdsClientFixtureV1(client);

        client.open(null, done);
    });
    
    suiteTeardown((done) => {
        client.close(null, done);
    });

    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

});
